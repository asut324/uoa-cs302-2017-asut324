import random, string, hashlib, cherrypy, urllib, urllib2, socket, json, sqlite3, datetime, calendar, time, os, threading, base64, mimetypes, os, shutil, cgi, database
from operator import itemgetter


# ---this file contains fuctions relating to database access---


def getFiles(user1, user2):		# gets all files between these two users
	conn = sqlite3.connect('data.db')
	
	ilist = []
	try:
		cursor = conn.execute("""SELECT * FROM FILES WHERE (sender=? AND destination=?) OR (sender=? AND destination=?)""",(user1,user2, user2,user1))
		for row in cursor:
			message_dict = {
				'sender' : row[1],
				'destination' : row[2],
				'file' : row[3],
				'filename' : row[4],		# Format a dict 
				'content_type' : row[5],
				'stamp' : row[6],
				'encryption' : row[7],
				'hashing' : row[8],
				'hash' : row[9],
				'decryptionkey' : row[10]
			}
			ilist.append(message_dict)		# create a list of dicts with messages in both directions 
	except: 
		return 4
	conn.close()

	ilist2 = sorted(ilist, key=itemgetter("stamp")) 	#sort by date

	return ilist2

def updateUserlist(input_dict):		# Refresh our user list, adding new users and updating the data of exisiting
	conn = sqlite3.connect('data.db')

	conn.execute("""UPDATE USERS SET LOGGEDIN = '0'""")	 # Set all to offline by default
	for x in xrange(0,len(input_dict)):  
		conn.executemany("REPLACE INTO USERS VALUES (?, ?, ?, ?, ?, ?)", [(input_dict[str(x)]["username"], input_dict[str(x)]["ip"], 
			input_dict[str(x)]["location"], input_dict[str(x)]["lastLogin"], input_dict[str(x)]["port"], "1")])				# replace info for this user
	conn.commit()		
	conn.close()
	return 0;	

def storeMessage(input_dict, isSent):		# This places a message in our database, we take a 'isSent' bool to store this message for offline messaging if it isnt sent
	conn = sqlite3.connect('data.db')
	conn.execute('INSERT INTO MESSAGES VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
		[None, 
		input_dict["sender"],
		input_dict["destination"],
		input_dict["message"],		# construct a dict
		input_dict["stamp"],
		input_dict["markdown"] if ("markdown" in input_dict) else 0,
		input_dict["encoding"] if ("encoding" in input_dict) else 0,
		input_dict["encryption"] if ("encryption" in input_dict) else 0,
		input_dict["hashing"] if ("hashing" in input_dict) else 0,
		input_dict["hash"] if ("hash" in input_dict) else None,
		int(isSent)]);		
	conn.commit()
	conn.close()
	return 0;

def storeFile(input_dict):		# This places a file in our database
	conn = sqlite3.connect('data.db')
	conn.execute('INSERT INTO FILES VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
		[None, 
		input_dict["sender"],
		input_dict["destination"],
		input_dict["file"],
		input_dict["filename"],
		input_dict["content_type"],		# consruct the dict
		input_dict["stamp"],
		input_dict["encryption"] if ("encryption" in input_dict) else 0,
		input_dict["hashing"] if ("hashing" in input_dict) else 0,
		input_dict["hash"] if ("hash" in input_dict) else None,
		input_dict["decryptionKey"] if ("decryptionKey" in input_dict) else None]);
	conn.commit()
	conn.close()
	return 0;
def fetchAdress(username):		# This fetches the ip and port (in a 2 part list) of a username from the databse
	conn = sqlite3.connect('data.db')
	try:
		cursor = conn.execute("""SELECT ip, port FROM USERS WHERE username=?""",(username,))
		row = cursor.fetchone()
		ip = str(row[0])			# construct return list
		port = str(row[1])
	except: 
		return 4
	conn.close()
	return [ip, port]

def fetchOnline():				# Get a list of all online user's usernames
	conn = sqlite3.connect('data.db')
	try:
		cursor = conn.execute("""SELECT username FROM USERS WHERE loggedin=?""",("1"))
		output = []
		for row in cursor:
			if row != None:
				output.append(row)		# create a list
	except:
		conn.close()
		return 4;
	conn.close()
	return output

def fetchProfileInfo(username): 	# Retrieve data from the database relating to profile info
	conn = sqlite3.connect('data.db')
	try:
		cursor = conn.execute("""SELECT * FROM PROFILES WHERE username=?""",(username,))
		row = cursor.fetchone()
		output_dict = {	
			'fullname' : str(row[1]),
			'position' : str(row[2]),
			'description' : str(row[3]),				# Get all user data
			'location' : str(row[4]),
			'picture' : str(row[5]),
			'encoding' : str(row[6]),
			'encryption' : str(row[7]),
			'decryptionKey' : str(row[8])
		}

	except: 
		conn.close()
		return 4  					# No data for that user
	conn.close()
	return output_dict

def createHash(input, standard = 0): 		# Create a hash of a particular string
	global SALT;

	if standard == 0: #No Hashing
		return None
	elif standard == 1:	#SHA-256 with no salt		
		hash_object = hashlib.sha256(input.encode('utf-8'))
	elif standard == 2:	 # SHA-256 message w/ salt
		hash_object = hashlib.sha256(input.encode('utf-8') + SALT)
	elif standard == 3:	 # SHA-512 with no salt
		hash_object = hashlib.sha512(input.encode('utf-8'))
	elif standard == 4:	 # SHA-512 message w/ salt
		hash_object = hashlib.sha256(input.encode('utf-8') + SALT)
	else:
		return None

	hex_dig = hash_object.hexdigest()
	return hex_dig

def hashCheck(input_dict):				# compare the hash given with the hash associated with that message in the databse
	conn = sqlite3.connect('data.db')
	try:
		cursor = conn.execute("""SELECT * FROM MESSAGES WHERE (STAMP=? AND SENDER=?) """,(input_dict["stamp"],input_dict["sender"]))
		row = cursor.fetchone()
		conn.close()
		if (createHash(row[3],int(input_dict["hashing"])) == input_dict["hash"]):
			return 0
		else:
			return 7	# hash does not match
	except: 
		return 4 	# message not found


def updateProfileInfo(username, input_dict): 	# Insert/Replace data for a user
	conn = sqlite3.connect('data.db') 
	try:
		if (input_dict["encryption"] not in {None, 0, '0', 'None'}):
			return 4;
	except: pass
	if ('cdn' in input_dict["picture"] or input_dict["picture"] == ""):	# this domain was loading incredibly slowly for some user images so I felt the need to hardcode it out...
		picture = None
	else:
		picture = input_dict["picture"]

	conn.executemany("REPLACE INTO PROFILES VALUES (?,?,?,?,?,?,?,?,?)", [( username, 
	input_dict["fullname"] , input_dict["position"] , input_dict["description"], 
	input_dict["location"], picture, 0, 
	0, None )])		
	conn.commit()				# insert/replace info
	conn.close()
	return 0;	

def fetchStatus(user):		# this gets the status associated with a current user
	conn = sqlite3.connect('data.db')
	try:
		cursor = conn.execute("""SELECT STATUS FROM STATUS WHERE USER=?""",(user,))
		row = cursor.fetchone()
		conn.close()
		return row[0]
	except: 
		conn.close()
		return str(None)   # No data for that user

def getMessages(user1, user2):		# gets all messages between these two users
	conn = sqlite3.connect('data.db')
	
	ilist = []
	try:
		cursor = conn.execute("""SELECT * FROM MESSAGES WHERE (sender=? AND destination=?) OR (sender=? AND destination=?)""",(user1,user2, user2,user1))
		for row in cursor:
			message_dict = {
				'sender' : row[1],
				'destination' : row[2],
				'message' : row[3],
				'stamp' : row[4],
				'markdown' : row[5],
				'encoding' : row[6],
				'encryption' : row[7],
				'hashing' : row[8],
				'hash' : row[9],
				'isSent' : row[10]
			}
			ilist.append(message_dict)		# create a list of dicts with messages in both directions 
	except: 
		conn.close()
		return 4
	conn.close()

	ilist2 = sorted(ilist, key=itemgetter("stamp")) 

	return ilist2

def addToBlacklist(user,target):		# add a username to the blocked user list for the current user
	try:
		conn = sqlite3.connect('data.db')
		conn.execute('INSERT INTO BLACKLIST VALUES (?, ?)', 
			[user, 
			target]);
		conn.commit()
		conn.close()
		return 0
	except:		# 		# Database error
		return 4

def removeAllFromBlacklist(user):		# remove all blocks for this user
	conn = sqlite3.connect('data.db')
	conn.execute('DELETE FROM BLACKLIST WHERE USER=?', 
		[user]);
	conn.commit()
	conn.close()		
	return 0


def getBlacklisted(username):	# get all usernames that this user has blocked 
	conn = sqlite3.connect('data.db')
	blocked_users = []
	cursor = conn.execute("""SELECT TARGET FROM BLACKLIST WHERE USER = ?""",(username,))	
	for row in cursor:
		if row is not None:
			blocked_users.append(row[0])

	return blocked_users


def isIPBlacklisted(username,connecting_ip):
	conn = sqlite3.connect('data.db')
	try:
		cursor = conn.execute("""SELECT USERNAME FROM USERS WHERE IP = ?""",(connecting_ip,))	# Find the username associated with that IP, this lets us see if an IP belongs to someone we blocked
		row = cursor.fetchone()
		ip_username = row[0]
		cursor = conn.execute("""SELECT * FROM BLACKLIST WHERE TARGET = ? and USER = ?""",(ip_username,username,))	# find entry for that used being blocked
		row = cursor.fetchone()
		conn.close()
		if row is None:
			return False		# if entry exists, IP is blacklisted
		else:
			return True
	except:
		conn.close()
		return False

def updateStatus(user, data):		#	Update the status for a user in the database
	conn = sqlite3.connect('data.db') 
	conn.executemany("REPLACE INTO STATUS VALUES (?,?)", [(user, data)])		
	conn.commit()		
	conn.close()
	return 0;	

def getUnsentFor(destination):		# get all messages destined for this user, that were not classes as being successfully sent
	conn = sqlite3.connect('data.db')
	ilist = []
	try:
		cursor = conn.execute("""SELECT * FROM MESSAGES WHERE ISSENT=0 AND destination=?""",(destination,))
		for row in cursor:
			message_dict = {
				'sender' : row[1],
				'destination' : row[2],
				'message' : row[3],				
				'stamp' : row[4],
				'markdown' : row[5],
				'encoding' : row[6],
				'encryption' : row[7],
				'hashing' : row[8],
				'hash' : row[9],
			}
			ilist.append(message_dict)		# create a list of dicts with messages 
	except: 
		conn.close()
		return 4
	conn.close()

	ilist2 = sorted(ilist, key=itemgetter("stamp")) 

	return ilist2	

def changeToSent(destination,stamp):		# set a message to sent if we manage to send it at a later point
	conn = sqlite3.connect('data.db')
	conn.execute("""UPDATE MESSAGES SET ISSENT = '1' WHERE DESTINATION=? and STAMP=?""",(destination,stamp))	 # Set message to sent	
	conn.commit()		
	conn.close()
	return 0;	
