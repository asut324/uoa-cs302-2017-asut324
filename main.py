import random, string, hashlib, cherrypy, urllib, urllib2, socket, json, sqlite3, datetime, calendar, time, os, threading, base64, mimetypes, os, shutil, cgi, database
from operator import itemgetter
from collections import defaultdict 

api_requests = defaultdict(int)
SALT = "COMPSYS302-2017"
port = 10001
chatting_with = None;
new_message_flag = False;	
userConnected = False;
online = False;
user = "default"

def checkRateLimit(ip):					# Increment the IP rate useage by 1, and check if it has passed the limit
	global api_requests, ip_address
	try:
		if (ip != ip_address):		# our own IP is not limited
			api_requests[ip] += 1
	except:
		api_requests[ip] += 1
	if api_requests[ip] > 12:	# Check total requests for that IP
		return True				# True idicates the limit is reached
	return False

def checkIP(ip):		# we check if an IP is rated limited or blacklisted
	global user
	print "Checking IP " + ip + "..."
	if checkRateLimit(ip):
		print "IP " + ip + " BLOCKED due to rate limit\n"	#check rate limit
		return False		# rate limit

	if database.isIPBlacklisted(user,ip):		# True means IP is allowed to access API
		print "IP " + ip + " BLOCKED due to blacklist\n"
		return False		# blacklist

	else:
		return True

def rateLimitService():			# rate limit thread, this is seperate from other threads as it runs  even if no one is logged on
	global api_requests		# rate limit is on a per-minute basis, so clear every 60 seconds
	while True:				
		print "\nClearing rate limit\n"
		time.sleep(60)
		api_requests.clear()


def createHash(input, standard = 0): # Create hash for given input, according to standard provided
	global SALT;

	if standard == 0: #No Hashing
		return None
	elif standard == 1:	#SHA-256 with no salt		
		hash_object = hashlib.sha256(input.encode('utf-8'))
	elif standard == 2:	 # SHA-256 message/file w/ salt
		hash_object = hashlib.sha256(input.encode('utf-8') + SALT)
	elif standard == 3:	 # SHA-512 with no salt
		hash_object = hashlib.sha512(input.encode('utf-8'))
	elif standard == 4:	 # SHA-512 message/file w/ salt
		hash_object = hashlib.sha256(input.encode('utf-8') + SALT)
	else:
		return None

	hex_dig = hash_object.hexdigest()
	return hex_dig

def getContentType(path_string):		# get the mimetype of a file
	
	url = urllib.pathname2url(path_string)
	return mimetypes.guess_type(url)


def encodeFile(path_string):			# base64 encode a file
	
	raw_file = open(path_string, 'rb') # Open binary in read only

	raw_file_read = raw_file.read()
	raw_file_encode = base64.encodestring(raw_file_read)
	return raw_file_encode				# return result

def decodeFile(file64):				# reverses a base64 encode
	return base64.decodestring(file64)


def cacheFiles(user1, user2):		# this decodes images from a database and puts them in a temp directory 
	map( os.unlink, (os.path.join( './public/temp/',f) for f in os.listdir('./public/temp/')) ) # clear cache 

	file_data = database.getFiles(user1, user2)		# get files from database

	for x in xrange(0,len(file_data)):
		file_result = open('./public/temp/' + file_data[x]['filename'], 'wb') # create a writable image and write the decoding result
		file_result.write(decodeFile(file_data[x]['file']))
	return

def makeURLrequest(url,data):			# Get data from url and arguments and make a standard request (Non json)
	values = urllib.urlencode(data)

	print "MAKING REQUEST TO " + url
	full_url = url + '?' + values
	response = urllib2.urlopen(full_url, timeout = 2)
	result = response.read()
	print "Result was: " + str(result[0:2])
	return result						# get result

def refreshUsers(username,password):		# ask the login server for users logged in
	global port, ip_address

	try: 
		url = 'http://cs302.pythonanywhere.com/getList'		# make dict of our info 
		data = {'username' : username,
				'password' : password,
				'enc' : '0',
				'json' : '1'}
	except KeyError:
		return '2';

	
	try:
		result = makeURLrequest(url,data)		# try storing the data
		database.updateUserlist(json.loads(result))
	except:
		pass
	return '0';

def sendFile(user_file_upload):			# Send a file by calling a users /receiveFile API
		global chatting_with

		try:
			cherrypy.session['username']	# only available if logged in
		except KeyError:
			return '1'


		data = database.fetchAdress(chatting_with)
		if (data == 4):						# no address for that user
			return data


		try:
			encoded_file = encodeFile(user_file_upload)
			input_dict = {
				'sender' : cherrypy.session['username'],
				'destination' : chatting_with,
				'file'	:	encoded_file,
				'filename' : os.path.basename(user_file_upload),
				'content_type' : getContentType(user_file_upload)[0],
				'stamp' : float(time.time()),				# create dict
				'encryption' : 0,
				'hashing' : 1,
				'hash' : createHash(encoded_file,1),
				'decryptionKey' : None
			}
		except:
			return '1';

		database.storeFile(input_dict)

		new_data = json.dumps(input_dict)				# send file and create a local copy
		url = "http://" + data[0] + ":" + data[1] + "/receiveFile"
		req = urllib2.Request(url, new_data, {'Content-Type': 'application/json'})
		try:
			response = urllib2.urlopen(req, timeout = 8)
			result = response.read()
		except:
			return '3'		# server unavailable

		return result

def sendMessage(destination, message):		# Send a message by calling a users /receiveMessage API
	data = database.fetchAdress(destination)
	if (data == 4):						# no adress for that user
		return data

	input_dict = {'sender' : cherrypy.session['username'],
			'destination' : destination,
			'message' : message,
			'stamp' : float(time.time()),
			'markdown' : 0,
			'encoding' : 0,					# create ditct
			'encryption' : 0,
			'hashing' : 1,
			'hash' : createHash(message, 1),
			'decryptionKey' : None}

	new_data = json.dumps(input_dict)
	url = "http://" + data[0] + ":" + data[1] + "/receiveMessage"		# attempt to send
	req = urllib2.Request(url, new_data, {'Content-Type': 'application/json'})
	try:
		response = urllib2.urlopen(req, timeout = 3)
		result = response.read()

		print "I SENT A MESSAGE! RESULT BACK WAS " + result
		if (str(0) in str(result)):
			database.storeMessage(input_dict, True)		#  store the message no matter what, but flag it as unsent if we get a bad response or no respone
		else:
			database.storeMessage(input_dict, False)
	except:
		database.storeMessage(input_dict, False)
		return '3'		# server unavailable

	return result


def report(username, password):			# thread for reporting  to the login server
	global online, location, api_requests
	while online:

		time.sleep(45);

		print "\n Reporting \n"
		url = 'http://cs302.pythonanywhere.com/report'
		data = {'username' : username,
				'password' : password,		# Make report request
				'location' : location,
				'ip' : ip_address, 
				'port' : str(port)}

		result = makeURLrequest(url,data)

def bgThread(username,password):		# start threads for background info updating
	global online
	unsentRequestThread = threading.Thread(target = requestUnsentMessages, args =(username,password))	# run once on login to see if there are unsent messages for this client
	unsentRequestThread.start()

	while online:

		profileRequestThread = threading.Thread(target = requestProfiles, args =(username,password))
		statusRequestThread = threading.Thread(target = requestStatus, args =(username,password))

		profileRequestThread.start()				
		statusRequestThread.start()							#  every 60 seconds, run a thread asking for user profiles and status updates 

		time.sleep(60);

		refreshUsers(username,password);

def requestUnsentMessages(username,password):			# as other clients for unsent messages destined for this user 
	onlineUsers = database.fetchOnline()
	for x in xrange(0,len(onlineUsers)):
		try:
			data = database.fetchAdress(onlineUsers[x][0])
			print "asking " + onlineUsers[x][0] + " for any unsent messages for us"

			submit_dict = {'requestor' : username}
			new_data = json.dumps(submit_dict)
			url = "http://" + data[0] + ":" + data[1] + "/retrieveMessages"		# use the /retrieveMessage API
			req = urllib2.Request(url, new_data, {'Content-Type': 'application/json'})
			response = urllib2.urlopen(req, timeout = 2)
			result = response.read()
			print "Success! Result was " + str(result)
		except:
			pass
	return '0'

def requestProfiles(username,password):  #This asks every online client for their profile
	onlineUsers = database.fetchOnline()		# get a list of every online user

	try:
		for x in xrange(0,len(onlineUsers)):
			try:
				scrapeProfile(onlineUsers[x][0],onlineUsers[x][0], username)	# ask every online user for their profile
			except: pass
	except: pass
	return '0';

def requestStatus(username,password):		# thread for getting status from all users
	onlineUsers = database.fetchOnline()
	for x in xrange(0,len(onlineUsers)):
		try:
			data = database.fetchAdress(onlineUsers[x][0])
			print "asking " + onlineUsers[x][0] + " for their status"

			submit_dict = {'profile_username' : onlineUsers[x][0]}
			new_data = json.dumps(submit_dict)
			url = "http://" + data[0] + ":" + data[1] + "/getStatus"						#  use the /getStatus API and store results in databse if we get a response
			req = urllib2.Request(url, new_data, {'Content-Type': 'application/json'})
			response = urllib2.urlopen(req, timeout = 2)
			result = response.read()
			result = json.loads(result)
			database.updateStatus(onlineUsers[x][0], result["status"])
			print "Success!"
		except:
			pass

	return '0'


def scrapeProfile(username, destination, sender):  # This  allows us to ask another client for a client profile for our database

	print "Asking " + username + " for their profile"
	data = database.fetchAdress(destination)
	if (data == 4):						# no adress for that user
		return data

	submit_dict = {'profile_username' : username,
			'sender' : sender}

	new_data = json.dumps(submit_dict)
	url = "http://" + data[0] + ":" + data[1] + "/getProfile"		# get the user profile with the /getProfile API

	req = urllib2.Request(url, new_data, {'Content-Type': 'application/json'})
	try:
		response = urllib2.urlopen(req, timeout = 3)
		result = response.read()

		try:
			if result["encryption"] not in {0,'',"0",None,"None"}:	# dont store if encrypted 
				return '9'
		except: pass
		database.updateProfileInfo(username, json.loads(result))
		print "Success!"
		return '0'
		
	except:
		return '3'		# server unavailable




############ Cherypy pages ##############

class MainApp(object):

	def header(self):		# construct basic page header and footer
		return '''<html>
			<head>
				<title>Hello</title>
				<link href ="https://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet">
				<link rel = "stylesheet"
					type = "text/css"
					href = "static/css/style.css" />
				<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
				<script src="static/js/main.js"></script>
				<script src="static/js/simpleUpload.min.js"></script>

			</head>
			<body>
				<div id = "navbar" class = "navbar">
					<div class = "logobox">
						<h2 style="font-size: 22px" class = "myTitle" id = "title"><b>InseChat</h2>
					</div>
					<div class = buttonBox>
						<ul class = ulBox>
						  <li><a href="/messaging">Messaging</a></li>
						  <li><a href="/profile">Profile</a></li>			
						  <li><a href="/blacklist">Blacklist</a></li>
						  <li><a href="/logout">Log Out</a></li>
						</ul>
					</div>
				</div>
				<div id = "container">
					<div id = "content">
					'''


	def footer(self):
		return"""<html>

			</div>
			</div>
		  </body>
		</html>"""


	# Page not found page
	@cherrypy.expose
	def default(self, *args, **kwargs):
		"""Default page"""
		Page = self.header()
		Page += "404 Page not found"	# 404 error
		cherrypy.response.status = 404	
		Page += "</body>"
		return Page

	# 'homepage', never visible, either loggs user in or goes to messaging
	@cherrypy.expose
	def index(self):

		try:
			cherrypy.session['username']	# only available if logged in
		except KeyError:
			raise cherrypy.HTTPRedirect('/login')	

		raise cherrypy.HTTPRedirect('/messaging')		# redirect to messaging normally

	@cherrypy.expose
	def login(self):		# login page, providing basic input form for username and password
		return self.header() + """<html>
			<form method="post" action="executeLogIn">
				<input type="text" value="" name="username" />
				<input type="password" value="" name="password" />
				<button type="submit">Log in</button>
			</form>""" + self.footer();

	@cherrypy.expose
	def logout(self):	# send logout to the sernever, the user never sees this page
		global online		   
		try:
			url = 'http://cs302.pythonanywhere.com/logoff'
			data = {'username' : cherrypy.session['username'],
					'password' : cherrypy.session['password']}
		except KeyError:
			raise cherrypy.HTTPRedirect('/')
		else:
			result = makeURLrequest(url,data)
			if (str(result[0]) == '0'):
				online = False;
				cherrypy.lib.sessions.expire()
				raise cherrypy.HTTPRedirect('/')
				print("\n0: Logout was successful\n")
			else:
				raise cherrypy.HTTPRedirect('/logout')
				print("\nX: Logout was unsuccessful\n")

	@cherrypy.expose
	def blacklist(self):		# page for choosing blacklisted users
		try:
			cherrypy.session['username']
		except:
			raise cherrypy.HTTPRedirect('/')

		Page = self.header()
		Page += """<div class='blackbox'>
					<form action="/logBlocked">
 						Edit your blocked users. Enter UPI in a comma seperated list:<br></br>
 						<textarea name="blockedList" value="Mickey" cols="40" rows="5"">"""
 		try:
 			for x in (database.getBlacklisted(cherrypy.session['username'])):	# show current list
 				Page += x + ", "
 		except:
 			pass
 		Page+=				"""</textArea>
  						<br>
  						<input type="submit" value="Submit">
					</form> 

				</div"""
		return Page + self.footer()

	@cherrypy.expose			# this adds the info from /blacklist to the database and refreshes the page
	def logBlocked(self, blockedList):	
		try:
			cherrypy.session['username']		# page not available if not logged in
		except:
			raise cherrypy.HTTPRedirect('/')
		try:
			database.removeAllFromBlacklist(cherrypy.session['username'])
			blockedListL = blockedList.replace(" ", "").split(",")
			print "\n\n" + str(blockedListL) + "\n\n"
			for x in blockedListL:
				if x not in (None,""):
					database.addToBlacklist(cherrypy.session['username'],x)
		except:
			pass

		raise cherrypy.HTTPRedirect('/blacklist')

	@cherrypy.expose			# page for messaging, main page
	def messaging(self, with_user= None):
		global chatting_with, new_message_flag, userConnected
		new_message_flag = False

		try:
			cherrypy.session['username']		# redirect if not logged in
		except:
			raise cherrypy.HTTPRedirect('/')

		if (with_user != chatting_with):		# update the profile when we open the chat for this user first time
			
			try:
				adress = database.fetchAdress(with_user) 
				url = 'http://' + adress[0] + ':' + adress[1] + '/ping'
				
				data = {'sender' : cherrypy.session['username']}
				
				makeURLrequest(url,data)				# try pinging the user to confirm connection
			except:
				userConnected  = False
			else:
				scrapeProfile(with_user,with_user,cherrypy.session['username'])
				userConnected = True;

			chatting_with = with_user

		try:
			messageData = database.getMessages(cherrypy.session['username'], with_user)
			fileData = database.getFiles(cherrypy.session['username'], with_user)
			totalData = sorted((messageData + fileData), key=itemgetter("stamp"))		# prepare data in chat, get files and messages and sort by stamp

			cacheFiles(cherrypy.session['username'], with_user)
		except KeyError:
			raise cherrypy.HTTPRedirect('/')

		Page = self.header()
		Page += "<div class='parent-container'>"

		# Left hand user bar #

		conn = sqlite3.connect('data.db')										
		cursor = conn.execute("SELECT username, lastlogin, loggedin  from USERS")
		
		Page += "<div class='user-container'>"
		Page += 	"<div class='user-content'>"
		for row in sorted(cursor, key=itemgetter(1), reverse=True):
			
			Page +=		"<a style='text-decoration: none' href='" + "./messaging?with_user=" + row[0] + "'>"
			Page += """		<div class='user-box"""
			try:
				status = str(database.fetchStatus(row[0])).replace(" ", "-")							# insert user data for each 'box'
				Page +=	" " + status
			except: pass
			Page  +=			"""'>
								<div class='user-photo'>
									<img src='""" 
			try:
				Page += str(database.fetchProfileInfo(row[0])["picture"]) 
			except:
				pass;
			Page +=					"""' onError="this.onerror=null;this.src='./static/default/placeholder.png';">
								</div>
								<div class='text-container'>
									<h3 class='titletext"""
			if (int(time.time()) - int(row[1]) <= 120):
				Page += " " + "online'"
			elif (int(time.time()) - int(row[1]) <= (5 * 120)):				# set text color for 2min: online, 5min: away
				Page += " " + "away'"
			else:
				Page += " " + "offline'"

			Page +=	""">""" 
			try:						
				Page += str(database.fetchProfileInfo(row[0])["fullname"]) 			# use full name, otherwise just username
			except:
				Page +=	str(row[0])
			Page +=					"""</h3>
									<h5 class='onlinetext'>Last Login: """
			Page +=						datetime.datetime.fromtimestamp(row[1]).strftime('%H:%M %d/%m')
			Page +=					"""</h5>
								</div>
							</div>
						</a>"""			
		conn.close()
		Page += 	"</div>"
		Page += "</div>"	

		### Center Bar ###
		Page += """<div class ='messaging-container' align="center" onload="setScrollPos()"">"""
		
		if with_user != None:
			
			Page += """<html>
				<div class='messaging-header'>
					<h3>"""

			Page += "Chatting with " 

			try: Page += str(database.fetchProfileInfo(with_user)["fullname"])		# use full name, otherwise just username
			except: Page += str(with_user)

			if (userConnected):
				Page += """<span style="color: green">      |  User Connected</span>"""

			Page += """</h3>
				</div>
				<div class='chat-messages' id='chat-messagebox'>"""

			for x in xrange(0,len(totalData)):			# run for each message to display

				try:
					message = totalData[x]["message"]
					if (totalData[x]["sender"] == cherrypy.session['username']):		# if our message is a text message, set it to sent or unsent for visual purposes
						messageHTML = """<div class='chat self"""
						if(totalData[x]["isSent"] == 0):
							messageHTML += " unsent'>"
						else:
							messageHTML += "'>"
						messageHTML+= """<div class="user-photo">"""
						try: messageHTML+= """<img src='""" + str(database.fetchProfileInfo(cherrypy.session['username'])["picture"]) +  """' onError="this.onerror=null;this.src='./static/default/placeholder.png';">"""
						except: pass
					else:
						messageHTML = """<div class="chat other">"""		# set color depending on if its a sent or recieved message
						messageHTML+= """<div class="user-photo">"""
						try: messageHTML+= """<img src='""" + str(database.fetchProfileInfo(with_user)["picture"]) +  """' onError="this.onerror=null;this.src='./static/default/placeholder.png';">"""
						except: pass

					messageHTML+= """</div>"""
					messageHTML+= """<p class="chat-message">"""
					messageHTML+= totalData[x]["message"]
					messageHTML+= """</p></div>"""

					Page += messageHTML

				except:
					if totalData[x]['content_type'] in ("image/png", "image/gif", "image/jpeg"):		# if the file is an image, show it in the chat
						if (totalData[x]["sender"] == cherrypy.session['username']):
							fileHTML = """<div class="chat self file_img">"""
							fileHTML+= """<div class="user-photo">"""
							try: fileHTML+= """<img src='""" + str(database.fetchProfileInfo(cherrypy.session['username'])["picture"]) +  """' onError="this.onerror=null;this.src='./static/default/placeholder.png';">"""
							except: pass
						else:
							fileHTML = """<div class="chat other file_img">"""		# add image to container
							fileHTML+= """<div class="user-photo">"""
							try: fileHTML+= """<img src='""" + str(database.fetchProfileInfo(with_user)["picture"]) +  """' onError="this.onerror=null;this.src='./static/default/placeholder.png';">"""
							except: pass

						fileHTML+= """</div>
									<div class="img-container">"""
						fileHTML+= """<img src='static/temp/""" + str(totalData[x]["filename"]) + """'> 
									</div>
								</div>"""
					
					elif totalData[x]['content_type'] in ("video/mp4", "video/webm", "audio/mpeg", "audio/ogg", "audio/wav"):		# media files show as interactive players

						if (totalData[x]["sender"] == cherrypy.session['username']):
							fileHTML = """<div class="chat self file_multimedia">"""
							fileHTML+= """<div class="user-photo">"""
							try: fileHTML+= """<img src='""" + str(database.fetchProfileInfo(cherrypy.session['username'])["picture"]) +  """' onError="this.onerror=null;this.src='./static/default/placeholder.png';">"""
							except: pass
						else:
							fileHTML = """<div class="chat other file_multimedia">"""			# add user photo
							fileHTML+= """<div class="user-photo">"""
							try: fileHTML+= """<img src='""" + str(database.fetchProfileInfo(with_user)["picture"]) +  """' onError="this.onerror=null;this.src='./static/default/placeholder.png';">"""
							except: pass

						fileHTML += "</div>"
						if (totalData[x]['content_type'] in ("video/mp4", "video/webm")):
							fileHTML += """	<video controls class="video_player">
											  <source src='static/temp/""" + str(totalData[x]["filename"]) + """'>"""
							fileHTML += """</video>
											</div>"""										# set up either a video or audio player
						else:
							fileHTML += """	<audio controls class="audio_player">
											  <source src='static/temp/""" + str(totalData[x]["filename"]) + """'>"""
							fileHTML += """</video>
											</div>"""
					else:

						if (totalData[x]["sender"] == cherrypy.session['username']):
							fileHTML = """<div class="chat self file_link">"""
							fileHTML+= """<div class="user-photo">"""
							try: fileHTML+= """<img src='""" + str(database.fetchProfileInfo(cherrypy.session['username'])["picture"]) +  """' onError="this.onerror=null;this.src='./static/default/placeholder.png';">"""
							except: pass
						else:
							fileHTML = """<div class="chat other file_link">"""			# if its none of these files, show a link box
							fileHTML+= """<div class="user-photo">"""
							try: fileHTML+= """<img src='""" + str(database.fetchProfileInfo(with_user)["picture"]) +  """' onError="this.onerror=null;this.src='./static/default/placeholder.png';"createHash>"""
							except: pass

						fileHTML += """</div>
										<div class="link-container">"""
						fileHTML += """			<a class="hyperlink" href ='./static/temp/""" + str(totalData[x]["filename"]) + """'>""" + str(totalData[x]["filename"]) + """</a>
										</div>
									</div>"""


					Page += fileHTML

			Page +=	"""</div>

				<div class='chat-bottom'>
					<form enctype="multipart/form-data" method="post" action='submitMessage' id='chatForm'>
						<input type='text' id='message' name='message' value='' placeholder='Enter a message...'/>		
						<input type="file" style="display:none;" class="userFileButton" id="userFileButton" name="user_file"/>

						<input type = "hidden" name="destination" value='"""			# send message form


			Page += str(with_user)
			Page += """'/>
						<input type='submit' value='Send'/>
					</form>
					<input type="button" id="userButton" class="userButton" value="Choose a file..." onclick="document.getElementById('userFileButton').click();"/>
				
				</div>"""
		else :
			Page += """<h5>Click a chat to begin!</h5>"""
		Page+=	"""</div>"""


		### Right hand Bar ###

		Page += "<div class='profiles-container'>"""
		Page +=		"""<div class='profile-box'>
							<div class="user-photo">
								<img src='"""
		try: Page += database.fetchProfileInfo(with_user)['picture']
		except: pass
		Page +=					"""' onError="this.onerror=null;this.src='./static/default/placeholder.png';">
							</div>
							<div class="prof-text-box">
								<h4 class = 'chat-name'>"""
		try: Page += str(database.fetchProfileInfo(with_user)["fullname"])
		except: Page += str(with_user)
		Page +=				"""</h4>"""
		Page +=							"""<h4 class = 'chat-prof-info'><b>Position</b></br>"""
		try: Page += str(database.fetchProfileInfo(with_user)["position"])			# show profile info if it exists for that user
		except: Page += '-'		
		Page +=							"""</br><b>Location</b></br>"""
		try: Page += str(database.fetchProfileInfo(with_user)["location"])
		except: Page += '-'
		Page +=						"""</br><b>Description</b></br>"""
		try: Page += str(database.fetchProfileInfo(with_user)["description"])
		except: Page += '-'						
		Page +=						"""</br><b>Status</b></br>"""
		status = str(database.fetchStatus(with_user))
		if status != "None": Page += status
		else: Page += '-'						
		Page +=					"""</h4></div>"""
		Page +=	"</div>"


		Page += """<div class="status-box">
		<h6>Your current Status:</h6>
		  <form action="/newStatus" method="post">
		  	<input type="hidden" name="with_user" value='""" + str(with_user) + """'>	
		    <select name="status">"""
		typeList =  {"Online", "Idle", "Away", "Do Not Disturb", "Offline"}		# area for setting status
		currentStatus = database.fetchStatus(cherrypy.session['username'])		# show our current status
		for types in typeList:
		    Page += """<option """
		    if types == currentStatus:
		    	Page+= "selected='selected' "
		    Page += "value='" + types + "'>" + types + "</option>"   
		Page += """</select>
		    <br>
		    <input class='sub_btn' type="Submit">
		  </form>"""

		

		return Page +"</div>" + self.footer();

	@cherrypy.expose				# redirect page for changing our status and refreshing
	def newStatus(self, status, with_user):
		database.updateStatus(cherrypy.session['username'],status)
		if with_user != "None":
			raise cherrypy.HTTPRedirect('/messaging?with_user=' + with_user)
		else: raise cherrypy.HTTPRedirect('/messaging')		# return to previous page


	@cherrypy.expose
	def executeLogIn(self, username, password):		# internal redirect page for completing login
		global port, ip_address, online, location, user

		hashedPassword = createHash(password, 2)
		url = 'http://cs302.pythonanywhere.com/report'
		data = {'username' : username,
				'password' : hashedPassword,			# hash password and create dict
				'location' : location,
				'ip' : ip_address, 
				'port' : str(port)}

		result = makeURLrequest(url,data)			# send to login server

		print "login result was " + str(result[0]) + "\n"

		if (str(result[0]) == '0'):
			user = username
			cherrypy.session['username'] = username
			cherrypy.session['password'] = hashedPassword			# set session variables

			online = True
			refreshUsers(cherrypy.session['username'],cherrypy.session['password']);

			report_thread = threading.Thread(target = report, args=(cherrypy.session['username'], cherrypy.session['password']))		# start thread of 45 second reporting
			report_thread.start()

			bgThreadService = threading.Thread(target = bgThread, args =(cherrypy.session['username'],cherrypy.session['password']))	# start thread that runs checks and updates on user data
			bgThreadService.start()

			raise cherrypy.HTTPRedirect('/')			# send us to the index
			print("\n0: Login was successful\n")
		else:
			raise cherrypy.HTTPRedirect('/login')
			print("\nX: Login was unsuccessful\n")


	@cherrypy.expose
	def new_message_query(self):		# query flag for the browser to refresh
		global new_message_flag
		if (new_message_flag == True):		# return 0 if there is a new message from this person 
			return '0'
		else:
			return '1'


	@cherrypy.expose
	def profile(self):
		
		try:
			user = cherrypy.session['username']   # We can only access profiles if we are logged in
		except KeyError:
			raise cherrypy.HTTPRedirect('/')

		userData = database.fetchProfileInfo(user)
		if (userData == 4):
			return self.header() +  """<html>
				</br><h2>You don't have a profile yet!</h2>				
				</br><a href='editProfile'>Create a profile</a></br></br>""" + self.footer()  # prompt the user to create a profile if none exists

		Page = self.header()
		Page += """<html>
				<div class ='profile-container' align="left">
					<div class='profile-header'>
						<h3>"""
		Page += cherrypy.session['username'] + "'s Profile"		# import username for the title

		Page += """<html></h3>
					</div>

					<div class='data-container'>

						<div class='profile-top'>
							<div class="user-photo"><img src=\""""		# get users image data
		Page += userData["picture"]

		Page += """\" onError="this.onerror=null;this.src='./static/default/placeholder.png';"></div>
							<p class="full-name">"""
		try: Page += str(database.fetchProfileInfo(cherrypy.session['username'])["fullname"])
		except: Page += str(cherrypy.session['username'])
		Page += """</p>
						</div>

						<div class='other-info'>"""
		Page +=	"<b>Position</b></br>" + userData["position"] + "</br></br>"
		Page +=	"<b>Description</b></br>" + userData["description"] + "</br></br>"
		Page +=	"<b>Location</b></br>" + userData["location"] + "</br>"		# General user data
		Page += """<html></div>
					</div>
				</div>"""

		return Page +"""</br><a href='editProfile'>Edit Profile</a>""" + self.footer();

	@cherrypy.expose
	def editProfile(self):			# This screen is for users wanting to edit or create a profile
		try:
			cherrypy.session['username']
		except:
			raise cherrypy.HTTPRedirect('/')

		Page = self.header()
		Page += """<div class='prof-edit-box'>
					<form action="/profileUpdater" method='post'>
 						<h6>Full name:</h6>
 						<input type="text" class='editbox' name="fullname" value='"""
 		try: Page += str(database.fetchProfileInfo(cherrypy.session['username'])["fullname"])
		except: pass			
 		Page +=				"""''>
 						<br><h6>Position:</h6>
 						<input type="text" class='editbox' name="position" value='"""
 		try: Page += str(database.fetchProfileInfo(cherrypy.session['username'])["position"])
		except: pass						
 		Page +=				"""''>
 						<br><h6>Location:</h6>			
 						<input type="text" class='editbox' name="location" value='"""
 		try: Page += str(database.fetchProfileInfo(cherrypy.session['username'])["location"])	# boxes show current known data
		except: pass						
 		Page +=				"""''>
 						<br><h6>Description:</h6>
 						<input type="text" class='editbox' name="description" value='"""
 		try: Page += str(database.fetchProfileInfo(cherrypy.session['username'])["description"])
		except: pass						
 		Page +=				"""''>
 						<br><h6>Picture URL:</h6>
 						<input type="text" class='editbox' name="picture" value='"""
 		try: Page += str(database.fetchProfileInfo(cherrypy.session['username'])["picture"])
		except: pass		
 		Page +=				"""''>

  						<br><br><input type="submit" value="Submit">
					</form> 

				</div"""
		return Page + self.footer()

	@cherrypy.expose		# apply changes and refresh page back to the profile page
	def profileUpdater(self, fullname, position, location, description, picture):
		try:
			cherrypy.session['username']
		except:
			raise cherrypy.HTTPRedirect('/')

		input_dict = {
			'fullname' :	(fullname if fullname != '' else None),
			'position' :	(position if position != '' else None),
			'location' :	(location if location != '' else None),
			'description' :	(description if description != '' else None),
			'picture' :	(picture if picture != '' else None)
		}
		database.updateProfileInfo(cherrypy.session['username'],input_dict)		# send data to database

		raise cherrypy.HTTPRedirect('/profile')


	# basic handler for when a user sends a message vie the gui
	@cherrypy.expose
	def submitMessage(self, destination, message=None, user_file=None):
		
		try:
			cherrypy.session['username']	# only available if logged in
		except KeyError:
			raise cherrypy.HTTPRedirect('/')

		try:
			if (message != ""):
				sendMessage(destination, message)		# dont send empty messages
		except:
			pass

		try:
			myFile = user_file.file.read()
			file_result = open('./public/img/' + str(user_file.filename), 'wb') # create a writable image and write the decoding result
			file_result.write(myFile)
			file_result.close()
			sendFile('./public/img/' + str(user_file.filename))
			map( os.unlink, (os.path.join( './public/img/',f) for f in os.listdir('./public/img/')) ) # clear cache
		except:
			pass

		raise cherrypy.HTTPRedirect('/messaging?with_user=' + destination)		# back to message page
		


####### PROTOCOL API ######## 
	
	@cherrypy.expose
	def listAPI(self):
		if (checkIP(cherrypy.request.remote.ip) == False):	# ensure IP is not blocked or rate limited
			return '11'

		string = "/getPublicKey [sender] "
		string += "/receiveMessage [sender][destination][message][stamp][markdown(opt)][encryption(opt)][hashing(opt)][hash(opt)][decryptionKey(opt)] "
		string += "/getProfile [profile_username][sender] "
		string += "/ping [sender(opt)] "
		string += "/acknowledge [sender][stamp][hashing][hash] "
		string += "/receiveFile [sender][destination][file][filename][content_type][stamp][encryption(opt)][hashing(opt)][hash(opt)][decryptionKey(opt)] "
		string += "/retrieveMessages [requestor] "
		string += "/getStatus [profile_username] "
		string += "Encryption 0 "
		string += "Hashing 0 1 3"		# list API

		return string


	@cherrypy.expose
	@cherrypy.tools.json_in()
	@cherrypy.tools.json_out()
	def receiveMessage(self):   # This API allows another client to send this client a message
		if (checkIP(cherrypy.request.remote.ip) == False):	# ensure IP is not blocked or rate limited
			return 11

		global chatting_with, new_message_flag

		try:
			message = cherrypy.request.json  # If we cant read the json, then it isn't sent properly
		except:
			return 1

		try:
			if (int(message["hashing"]) not in {0, 1, 3}):
				return 10
		except:
			pass 								# make sure the hashing and encryption standards are supported

		try:
			if (int(message["encryption"]) != 0):
				return 9
		except:
			pass

		try:
			database.storeMessage(message, True)			# store the message and flip the global refresh flag if we are chatting with this user in the browser
			if (message["sender"] == chatting_with):
				new_message_flag = True;

		except:
			return 4

		return 0

	@cherrypy.expose
	@cherrypy.tools.json_in()
	@cherrypy.tools.json_out()	
	def receiveFile(self):     # This API allows another client to send this client a file
		global chatting_with, new_message_flag
		if (checkIP(cherrypy.request.remote.ip) == False):	# ensure IP is not blocked or rate limited
			return 11
		
		try:
			message = cherrypy.request.json         # If we cant read the json, then it isn't sent properly
		except:
			return 1

		try:
			if (int(message["hashing"]) not in {0, 1, 3}):
				return 10
		except:
			pass                                	# make sure the hashing and encryption standards are supported

		try:
			if (int(message["encryption"]) != 0):
				return 9
		except:
			pass

		try:
			database.storeFile(message)
			if (message["sender"] == chatting_with):			# store the message and flip the global refresh flag if we are chatting with this user in the browser
				new_message_flag = True;
		except:
			return 4

		return 0


	@cherrypy.expose
	@cherrypy.tools.json_in()
	@cherrypy.tools.json_out()
	def acknowledge(self):  #This API allows another client to reconfirm that the message was received correctly
		if (checkIP(cherrypy.request.remote.ip) == False):	# ensure IP is not blocked or rate limited
			return 11
		try:
			message = cherrypy.request.json 		 # If we cant read the json, then it isn't sent properly
		except:
			return 1

		if (int(message["hashing"]) not in {0, 1, 3}):		# hashing must be supported
			return 10

		return databse.hashCheck(message)		# return the results of the hashcheck

	@cherrypy.expose
	def ping(self, sender = None):				# allow clients to check if we are reachable
		if (checkIP(cherrypy.request.remote.ip) == False):
			return '11'

		return '0'

	@cherrypy.expose
	@cherrypy.tools.json_in()
	@cherrypy.tools.json_out()
	def getStatus(self):				# give other clients our status
		if (checkIP(cherrypy.request.remote.ip) == False):	# ensure IP is not blocked or rate limited
			return 11
		
		try:
			message = cherrypy.request.json
			user = message["profile_username"]
		except:
			return 1

		data = {
			'status' :	database.fetchStatus(user)	# return our info
		}
	
		return (data)

	@cherrypy.expose
	@cherrypy.tools.json_in()
	@cherrypy.tools.json_out()
	def getProfile(self):  #This API allows another client to request information about the user operating this client. 
		if (checkIP(cherrypy.request.remote.ip) == False):	# ensure IP is not blocked or rate limited
			return 11
		try:
			message = cherrypy.request.json
		except:
			return 1

		user = message["profile_username"]
		userData = database.fetchProfileInfo(user)

		return (userData)

	@cherrypy.expose
	@cherrypy.tools.json_in()
	@cherrypy.tools.json_out()
	def retrieveMessages(self):  #This API allows another client to request unsent messages for them 
		if (checkIP(cherrypy.request.remote.ip) == False):	# ensure IP is not blocked or rate limited
			return 11
		try:
			message = cherrypy.request.json
			destination = message["requestor"]
			locationData = database.fetchAdress(destination)
			if (locationData == 4):						# no address for that user
				return locationData

			url = "http://" + locationData[0] + ":" + locationData[1] + "/receiveMessage"
			result = database.getUnsentFor(destination)				# get unsent messages for that user
			for x in xrange(0,len(result)):						
				new_data = json.dumps(result[x])
				req = urllib2.Request(url, new_data, {'Content-Type': 'application/json'})
				try:
					response = urllib2.urlopen(req, timeout = 3)		# try sending each one
					code = response.read()
					print "I SENT A MESSAGE! RESULT BACK WAS " + code
					if (str(0) in str(code)):
						database.changeToSent(result[x]["destination"],result[x]["stamp"])
				except:
					break	# not getting the messages, so give up
		except:
			return 1
		return 0




if __name__ == '__main__':
	global ip_address, location
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.connect(("8.8.8.8", 80))					# get IP address
	ip_address = s.getsockname()[0]

	if (ip_address[0:3] == "172"):
		location = 1
	elif (ip_address[0:3] == "10."):		# set location depending on IP
		location = 0
	else:
		location = 2


	if not os.path.exists("./public/temp"):		# ensure we have neccessary directories
		os.makedirs("./public/temp")

	if not os.path.exists("./public/img"):
		os.makedirs("./public/img")


	conf = {
		'/': {
			'tools.sessions.on': True,
			'tools.staticdir.root': os.path.abspath(os.getcwd())
		},
		'/static': {
			'tools.staticdir.on': True,
			'tools.staticdir.dir': './public'
		}
	}
 
	cherrypy.config.update({'server.socket_host': ip_address,
							'server.socket_port': port,
							'engine.autoreload.on': True,
						   })

	rateLimitThread = threading.Thread(target = rateLimitService)		# start the rate limiting thread
	rateLimitThread.start()

	cherrypy.quickstart(MainApp(), '/', conf)
