import pytest, database, sqlite3, main

# this file tests database interactions using pytest 3.0


def test_one():		# Store a message and make sure it is there
	input_dict = {
		'sender' : u'me',
		'destination' : u'also me',
		'message' : u'hi me',
		'stamp' : 999999999,
		'markdown' : 0,
		'encoding' : 0,
		'encryption' : 0,
		'hashing' : 0,
		'hash' : None,
		'isSent' : 1
	}
	database.storeMessage(input_dict,True) 		#store message
	result = database.getMessages('me','also me')	# get the messages back
	resultFound = False
	for x in xrange(0,len(result)):
		if result[x] == input_dict:		# ensure a result matches
			resultFound = True

	assert (resultFound)

def test_two():			# ensure we can fetch the IP/Port of a user
	input_dict = {
		'username' : 'test',
		'ip' : '111.111.111.111',
		'location' : '0',
		'lastLogin' : '0',		# create a test user
		'port' : '11111',
		'isOnline' : '0'
	}
	userlist = {'0' : input_dict}		# add them to the database
	database.updateUserlist(userlist)
	result = database.fetchAdress('test')	# test we can get their adress
	assert (result[0] == input_dict["ip"] and result[1] == input_dict["port"])

def test_three():			# ensure we can fetch the full user information
	input_dict = {
		'fullname' : 'testing 123',
		'position' : 'test',
		'description' : 'test',
		'location' : 'test',		# create a test user
		'picture' : 'test.png',
		'encryption': '0', 
		'decryptionKey': 'None',
		'encoding': '0'
	}
	# add them to the database
	database.updateProfileInfo('test',input_dict)

	result = database.fetchProfileInfo('test')	# test we can get their info
	assert (result == input_dict)

def test_four():				#test hashing to make sure our hashing, storing and checking works
	message = u'hi me'

	input_dict = {
		'sender' : u'me',
		'destination' : u'also me',		# create a message
		'message' : message,
		'stamp' : 999999999,
		'markdown' : 0,
		'encoding' : 0,
		'encryption' : 0,
		'hashing' : 1,
		'hash' : main.createHash(message,1),
		'isSent' : 1
	}
	database.storeMessage(input_dict,True) 		#store message

	dict_two = {
	'sender' : u'me',
	'stamp'	:	'999999999',			# create a second message representing what me might get in an acknowledge call
	'hashing' :	1,
	'hash'	:	main.createHash(message,1)
	}
	assert(database.hashCheck(dict_two) == 0) 	# make sure this is correct and matches

	dict_three = {
	'sender' : u'me',
	'stamp'	:	'999999999',			# create a second message representing what me might get in an acknowledge call
	'hashing' :	1,
	'hash'	:	'9999999999999999999999'
	}
	assert(database.hashCheck(dict_three) == 7) # make sure this is fails the test

def test_five():			# test setting, chaning, and getting status
	user = 'test'
	data = 'Online'
	database.updateStatus(user,data)		# create an online status

	assert(database.fetchStatus(user) == 'Online')

	database.updateStatus(user,'Do Not Disturb')	# update the status

	assert(database.fetchStatus(user) != 'Online')	# status should have changed

def test_six():		# testing rate limiting

	for x in xrange(0,15):
		main.checkIP('111.111.111.111')	# call the check 16 times

	assert (main.checkIP('111.111.111.111') == False)	# ensure the IP gets blocked after making 8 successive calls


def test_seven():			# check blacklisting
	input_dict = {
		'username' : 'test',
		'ip' : '111.111.111.110',
		'location' : '0',
		'lastLogin' : '0',		# create a test user, this is for checking the IP of
		'port' : '11111',
		'isOnline' : '0'
	}
	userlist = {'0' : input_dict}		# add them to the database
	database.updateUserlist(userlist)

	database.addToBlacklist('test','test') 	# blacklist ourselves for test purposes

	assert(database.isIPBlacklisted('test','111.111.111.110')) # make sure test's IP is blocked

def test_eight():		# Test getting unsent messages
	input_dict = {
		'sender' : u'me',
		'destination' : u'you',
		'message' : u'hi me',
		'stamp' : 999999999,
		'markdown' : 0,
		'encoding' : 0,
		'encryption' : 0,
		'hashing' : 0,
		'hash' : None,
	}
	database.storeMessage(input_dict,False) 		#store message as if it was not sent
	result = database.getUnsentFor('you')	# get the messages back
	resultFound = False
	try:
		for x in xrange(0,len(result)):
			if result[x] == input_dict:		# ensure a result matches
				resultFound = True
	except:
		pass
	assert (resultFound)
