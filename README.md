# Austin Sutherland, asut324

- Open the Ubutnu terminal and navigate to this directory using "cd [filepath]"
- To run the program, simply type ./run_program

-Once the terminal says 'ENGINE bus STARTED', open your browser and type in the adress shown in the line "ENGINE Serving on [...]" and hit enter
-Type your credentials to log in.
-To edit the blacklist or your profile, click the options in the top right of the navbar. To return to the messaging screen, just click 'messaging'

- To run the PyTest tests, use the command ./run_tests instead
This should automatically install pytest


If a script says [permission denied], run 'chmod 755 [script_name]' and try again.
If it otherwise does not launch, delete the env folder and try again


Supported clients:

Sending/receiving files/messages:	aliv074, hone075

Offline Messaging:			abai196, myep112

User Status:				aliv074, hone075, jfan082

Unicode:				hone075, ramo588

Acknowledge hashing			ssit662, abah808
